// используемые переменные
const searchInput = document.querySelector('.search__input');
const searchForm = document.querySelector('.search__form');
const personContainer = document.querySelector('.person');

let searchInputValue = null;
let person = null;

// чистим контейнер с персонажем
function personClear() {
    const error = document.querySelector('.error');
    personContainer.innerHTML = '';
    if(error !== null) error.remove();
}

// если персонажа не существует
function emptyPerson() {
    personClear();
    const errorPerson = document.createElement('h6');
    errorPerson.className = 'error';
    errorPerson.innerHTML = 'Персонажа с таким ID не существует.';
    searchForm.before(errorPerson);
}


function drawInfoRow(label, value) {
    return `<div class="row"><p><label>${label}</label>${value}</p></div>`;
}

// отрисовка персонажа
function drawPerson(person) {
    personClear();
    const { name, height, weight, eyeColor, planet, gender } = person;
    const personInfo = document.createElement('div');
    personInfo.className = 'person__info';
    personInfo.innerHTML = drawInfoRow('Имя: ', name) + drawInfoRow('Планета: ', planet);

    const personIndicators = document.createElement('div');
    personIndicators.className = 'person__indicators';
    personIndicators.style.height = `${height}px`;
    personIndicators.style.width = `${weight}px`;
    personIndicators.style.background = eyeColor;
    personIndicators.style.borderRadius = gender == 'female' ? '25px' : '0px';

    personContainer.append(personInfo, personIndicators);
}

// запрос за персонажем
async function personRequest(id) {
    const personUrl = `https://swapi.dev/api/people/${id}`;
    let personResponse = await fetch(personUrl);
    let result = null;
        if (personResponse.ok) {
            let jsonPerson = await personResponse.json();
            const planetUrl = jsonPerson.homeworld;
            let planetResponse = await fetch(planetUrl);

            if (planetResponse.ok) {
                let jsonPlanet = await planetResponse.json();
                result = {
                    name: jsonPerson.name,
                    height: jsonPerson.height,
                    weight: jsonPerson.mass,
                    eyeColor: jsonPerson.eye_color,
                    planet: jsonPlanet.name,
                    gender: jsonPerson.gender
                };
            }
        } else {
            result = personResponse.status;
        }
    return result;
}

// обработчики событий
function searchInputOnChange(e) {
    const isInteger = /^[0-9]+$/;
    if (e.target.value === '' || isInteger.test(e.target.value)) {
        searchInputValue = e.target.value;
    }
    e.target.value = searchInputValue;
}

async function searchFormSubmit(e) {
    e.preventDefault();
    person = await personRequest(searchInputValue);
    if(person === 404 || person === 500) emptyPerson();
    else drawPerson(person);
}

// добавляем слушатели
searchInput.addEventListener("input", searchInputOnChange);
searchForm.addEventListener("submit", searchFormSubmit);